import { Injectable } from '@angular/core';
import {F1Racer} from "./model/F1-Racer";

  const F1Racers: F1Racer[] = [
    { name: "Lewis Hamilton", team: 'Petronas AMG', number:44 },
    { name: "Sebastian Vettel", team: 'Ferrari', number:5 },
    { name: "Kimi Raikonen", team: 'Ferrari', number:7 },
    { name: "Veltari Bottas", team: 'Petronas AMG', number:77 }
  ];

@Injectable()
export class F1RacerManagementService {
  f1racers: F1Racer[];
  constructor() {
    this.f1racers=F1Racers;
  }

  Metoda(): F1Racer[] {
    return this.f1racers;
  }

}
