import {Component, Input, OnInit} from '@angular/core';
import {F1Racer} from "../model/F1-Racer";

@Component({
  selector: 'app-f1-racer-details',
  templateUrl: './f1-racer-details.component.html',
  styleUrls: ['./f1-racer-details.component.css']
})
export class F1RacerDetailsComponent implements OnInit {

  constructor() { }
  @Input() f1Racer: F1Racer;
  ngOnInit() {
  }

}
