import { TestBed, inject } from '@angular/core/testing';

import { F1RacerManagementService } from './f1-racer-management.service';

describe('F1RacerManagementService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [F1RacerManagementService]
    });
  });

  it('should be created', inject([F1RacerManagementService], (service: F1RacerManagementService) => {
    expect(service).toBeTruthy();
  }));
});
