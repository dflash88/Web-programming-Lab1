import { Component } from '@angular/core';
import {F1Racer} from "./model/F1-Racer";
import {F1RacerManagementService} from "./f1-racer-management.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'F1 grid talk';
  f1racers: F1Racer[];
  selectedF1racer: F1Racer;
  onSelect(f1racer: F1Racer): void {
    this.selectedF1racer = f1racer;
  }
  constructor(f1RacersService: F1RacerManagementService){
      this.f1racers=f1RacersService.Metoda();
  }
}
